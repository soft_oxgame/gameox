/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.oxgameui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 66945
 */
public class TestWriteObject {
    public static void main(String[] args){
        File file =null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        Player X = new Player('X');
        Player O = new Player('O');
        
        X.win();
        O.lose();
        O.win();
        X.lose();
        System.out.println(X);
        System.out.println(O);
        try {
             file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos =new ObjectOutputStream(fos);
            oos.writeObject(X);
            oos.writeObject(O);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
